package taco.apkmirror.activities;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.net.Uri;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.IdRes;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.animation.DecelerateInterpolator;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabReselectListener;
import com.roughike.bottombar.OnTabSelectListener;

import im.delight.android.webview.AdvancedWebView;
import taco.apkmirror.R;
import taco.apkmirror.classes.PageAsync;
import taco.apkmirror.interfaces.AsyncResponse;

public class MainActivity extends AppCompatActivity implements AdvancedWebView.Listener, AsyncResponse {

    private AdvancedWebView webView;
    private ProgressBar progressBar;
    private BottomBar navigation;
    private FloatingActionButton fabSearch;

    private SwipeRefreshLayout refreshLayout;
    private RelativeLayout settingsLayoutFragment;
    private RelativeLayout webContainer;
    private FrameLayout progressBarContainer;
    private LinearLayout firstLoadingView;

    private static final String APKMIRROR_URL = "http://www.apkmirror.com/";
    private static final String APKMIRROR_UPLOAD_URL = "http://www.apkmirror.com/apk-upload/";

    Integer shortAnimDuration;

    Integer previsionThemeColor = Color.parseColor("#FF8B14");

    SharedPreferences sharedPreferences;

    private boolean settingsShortcut = false;
    private boolean triggerAction = true;

    NfcAdapter nfcAdapter;

    private OnTabSelectListener tabSelectListener = new OnTabSelectListener() {
        @Override
        public void onTabSelected(@IdRes int tabId) {
            if (triggerAction) {
                if (tabId == R.id.navigation_home) {
                    //Home pressed
                    if (settingsLayoutFragment.getVisibility() != View.VISIBLE) {
                        //settings is not visible
                        //Load url
                        webView.loadUrl(APKMIRROR_URL);
                    } else {
                        //settings is visible, gonna hide it
                        if (webView.getUrl().equals(APKMIRROR_UPLOAD_URL)) {
                            webView.loadUrl(APKMIRROR_URL);
                        }
                        crossFade(settingsLayoutFragment, webContainer);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            changeUIColor(previsionThemeColor);
                        }
                    }
                } else if (tabId == R.id.navigation_upload) {
                    //Upload pressed
                    if (settingsLayoutFragment.getVisibility() != View.VISIBLE) {
                        //settings is not visible
                        //Load url
                        webView.loadUrl(APKMIRROR_UPLOAD_URL);
                    } else {
                        //settings is visible, gonna hide it
                        if (!webView.getUrl().equals(APKMIRROR_UPLOAD_URL)) {
                            webView.loadUrl(APKMIRROR_UPLOAD_URL);
                        }
                        crossFade(settingsLayoutFragment, webContainer);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            changeUIColor(previsionThemeColor);
                        }
                    }
                } else if (tabId == R.id.navigation_settings) {
                    //Settings pressed
                    if (firstLoadingView.getVisibility() == View.VISIBLE) {
                        firstLoadingView.setVisibility(View.GONE);
                    }
                    crossFade(webContainer, settingsLayoutFragment);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        changeUIColor(ContextCompat.getColor(MainActivity.this,
                                R.color.colorPrimary));
                    }
                } else if (tabId == R.id.navigation_exit) {
                    finish();
                }
            }
            triggerAction = true;
        }
    };
    private WebChromeClient chromeClient = new WebChromeClient() {

        @Override
        public void onProgressChanged(WebView v, int progress) {
            // update the progressbar value
            ObjectAnimator oa = ObjectAnimator.ofInt(progressBar, "progress", progress);
            oa.setDuration(100); // 0.5 second
            oa.setInterpolator(new DecelerateInterpolator());
            oa.start();
        }
    };

    private void initNavigation() {
        //Making the bottom navigation do something
        navigation.setOnTabSelectListener(tabSelectListener);
        navigation.setOnTabReselectListener(tabReselectListener);

        if (sharedPreferences.getBoolean("show_exit", false)) {
            navigation.setItems(R.xml.navigation_exit);
            navigation.invalidate();
        }
    }

    private void initSearchFab() {
        Boolean fab = sharedPreferences.getBoolean("fab", true);
        if (fab) {
            fabSearch.show();
            fabSearch.setOnClickListener((View v) -> search());
        }
    }

    private void initWebView(String url) {
        webView.setListener(this, this);
        webView.addPermittedHostname("apkmirror.com");
        webView.setWebChromeClient(chromeClient);
        webView.setUploadableFileTypes("application/vnd.android.package-archive");
        webView.loadUrl(url);
        refreshLayout.setOnRefreshListener(() -> webView.reload());
    }

    @Override
    protected void onResume() {
        super.onResume();
        webView.onResume();
    }

    @Override
    protected void onPause() {
        webView.onPause();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        webView.onDestroy();
        super.onDestroy();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            //if (sharedPreferences.getBoolean("dark_mode", true)) {
            setTheme(R.style.AppTheme);
            //} else { setTheme(R.style.SplashTheme); }
            setContentView(R.layout.activity_main);

            previsionThemeColor = Color.parseColor("#FF8B14");

            //Views
            refreshLayout = findViewById(R.id.refresh_layout);
            progressBar = findViewById(R.id.main_progress_bar);
            navigation = findViewById(R.id.navigation);
            settingsLayoutFragment = findViewById(R.id.settings_layout_fragment);
            webContainer = findViewById(R.id.web_container);
            firstLoadingView = findViewById(R.id.first_loading_view);
            webView = findViewById(R.id.main_webview);
            fabSearch = findViewById(R.id.fab_search);
            progressBarContainer = findViewById(R.id.main_progress_bar_container);
            sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

            initSearchFab();
            nfcAdapter = NfcAdapter.getDefaultAdapter(this);
            shortAnimDuration = getResources().getInteger(android.R.integer.config_shortAnimTime);

            initNavigation();
            boolean saveUrl = sharedPreferences.getBoolean("save_url", false);
            String url;

            Intent link = getIntent();
            Uri data = link.getData();

            if (data != null) {
                //app was opened from browser
                url = data.toString();
            } else {
                //data is null which means it was either launched from shortcuts or normally
                Bundle bundle = link.getExtras();
                if (bundle == null) {
                    //Normal start from launcher
                    if (saveUrl) {
                        url = sharedPreferences.getString("last_url", APKMIRROR_URL);
                    } else {
                        url = APKMIRROR_URL;
                    }
                } else {
                    //Ok it was shortcuts, check if it was settings
                    String bU = bundle.getString("url"); // bU = bundleUrl
                    if (bU != null) {
                        if (bU.equals("apkmirror://settings")) {
                            //It was settings
                            url = APKMIRROR_URL;
                            navigation.selectTabWithId(R.id.navigation_settings);
                            crossFade(webContainer, settingsLayoutFragment);
                            settingsShortcut = true;
                        } else {
                            url = bU;
                        }
                    } else {
                        if (saveUrl) {
                            url = sharedPreferences.getString("last_url", APKMIRROR_URL);
                        } else {
                            url = APKMIRROR_URL;
                        }
                    }
                }
            }

            initWebView(url);

            // I know not the best solution xD
            if (!settingsShortcut) {
                firstLoadingView.setVisibility(View.VISIBLE);
                new Handler().postDelayed(() -> {
                    if (firstLoadingView.getVisibility() == View.VISIBLE) {
                        crossFade(firstLoadingView, webContainer);
                    }
                }, 2000);
            }

        } catch (final RuntimeException e) {

            new MaterialDialog.Builder(this)
                    .title(R.string.error)
                    .content(R.string.runtime_error_dialog_content)
                    .positiveText(android.R.string.ok)
                    .neutralText(R.string.copy_log)
                    .onPositive((dialog, which) -> finish())
                    .onNeutral((dialog, which) -> {
                        // Gets a handle to the clipboard service.
                        ClipboardManager clipboard = (ClipboardManager)
                                getSystemService(Context.CLIPBOARD_SERVICE);
                        // Creates a new text clip to put on the clipboard
                        ClipData clip = ClipData.newPlainText("log", e.toString());
                        if (clipboard != null) {
                            clipboard.setPrimaryClip(clip);
                        } else {
                            Toast.makeText(MainActivity.this, getString(R.string.clip_error),
                                    Toast.LENGTH_LONG).show();
                        }
                    }).show();
        }
    }

    @Override
    protected void onStop() {
        if (sharedPreferences.getBoolean("save_url", false) && !webView.getUrl()
                .equals("apkmirror://settings")) {
            sharedPreferences.edit().putString("last_url", webView.getUrl()).apply();
        }
        super.onStop();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent i) {
        super.onActivityResult(requestCode, resultCode, i);
        webView.onActivityResult(requestCode, resultCode, i);
    }

    @Override
    public void onBackPressed() {
        if (settingsLayoutFragment.getVisibility() != View.VISIBLE) {
            if (!webView.onBackPressed()) {
                return;
            }
        } else {
            crossFade(settingsLayoutFragment, webContainer);
            if (webView != null && webView.getUrl().equals(APKMIRROR_UPLOAD_URL)) {
                triggerAction = false;
                navigation.selectTabWithId(R.id.navigation_upload);
            } else {
                triggerAction = false;
                navigation.selectTabWithId(R.id.navigation_home);
            }
            return;
        }
        super.onBackPressed();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Next line causes crash
        //webView.saveState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        // Next line causes crash
        //webView.restoreState(savedInstanceState);
    }

    private OnTabReselectListener tabReselectListener = new OnTabReselectListener() {
        @Override
        public void onTabReSelected(@IdRes int tabId) {
            Integer webScrollY = webView.getScrollY();

            if (tabId == R.id.navigation_home) {
                //Home re-pressed
                if (webScrollY != 0) {
                    //Scroll to top
                    webView.setScrollY(0);
                } else {
                    //Load url
                    webView.loadUrl(APKMIRROR_URL);
                }
            } else if (tabId == R.id.navigation_upload) {
                //Upload re-pressed
                if (webScrollY != 0) {
                    //Scroll to top
                    webView.setScrollY(0);
                } else {
                    //Load url
                    webView.loadUrl(APKMIRROR_UPLOAD_URL);
                }
            }
        }
    };

    public void runAsync(String url) {
        //getting apps
        PageAsync p = new PageAsync(); // p = pageAsync
        p.response = MainActivity.this;
        p.execute(url);
    }

    private void search() {
        new MaterialDialog.Builder(this)
                .title(R.string.search)
                .inputRange(1, 100)
                .input(R.string.search, R.string.nothing, (dialog, input) -> {
                })
                .onPositive((dialog, which) -> {
                    if (dialog.getInputEditText() != null) {
                        webView.loadUrl("http://www.apkmirror.com/?s=" + dialog.getInputEditText().getText());
                    } else {
                        Toast.makeText(MainActivity.this, getString(R.string.search_error),
                                Toast.LENGTH_SHORT).show();
                    }
                })
                .negativeText(android.R.string.cancel)
                .show();
    }

    private void crossFade(final View toHide, View toShow) {

        toShow.setAlpha(0f);
        toShow.setVisibility(View.VISIBLE);

        toShow.animate()
                .alpha(1f)
                .setDuration(shortAnimDuration)
                .setListener(null);

        toHide.animate()
                .alpha(0f)
                .setDuration(shortAnimDuration)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator a) {
                        toHide.setVisibility(View.GONE);
                    }
                });
    }

    private void download(String url, String name) {
        if (!sharedPreferences.getBoolean("external_download", false)) {
            if (AdvancedWebView.handleDownload(this, url, name)) {
                Toast.makeText(MainActivity.this, getString(R.string.download_started),
                        Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(MainActivity.this, getString(R.string.cant_download),
                        Toast.LENGTH_SHORT).show();
            }
        } else {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
        }
    }

    private boolean isWritePermissionGranted() {
        return Build.VERSION.SDK_INT < 23 ||
                checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onProcessFinish(Integer themeColor) {

        // updating interface
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            changeUIColor(themeColor);
        }
        previsionThemeColor = themeColor;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void changeUIColor(Integer color) {

        ValueAnimator a = ValueAnimator.ofArgb(previsionThemeColor, color); // a = anim
        a.setEvaluator(new ArgbEvaluator());

        a.addUpdateListener((ValueAnimator vA) -> { // vA = valueAnimator
            progressBar.getProgressDrawable().setColorFilter(new LightingColorFilter(0xFF000000,
                    (Integer) vA.getAnimatedValue()));
            setSystemBarColor((Integer) vA.getAnimatedValue());
            navigation.setActiveTabColor((Integer) vA.getAnimatedValue());
            fabSearch.setBackgroundTintList(ColorStateList.valueOf((Integer)
                    vA.getAnimatedValue()));
        });

        a.setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime));
        a.start();
        refreshLayout.setColorSchemeColors(color, color, color);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void setSystemBarColor(int color) {
        int clr;
        //this makes the color darker or use a nicer orange color
        if (color != Color.parseColor("#FF8B14")) {
            float[] hsv = new float[3];
            Color.colorToHSV(color, hsv);
            hsv[2] *= 0.8f;
            clr = Color.HSVToColor(hsv);
        } else {
            clr = Color.parseColor("#F47D20");
        }
        Window w = MainActivity.this.getWindow();
        w.setStatusBarColor(clr);
    }

    private void setupNFC(String url) {
        if (nfcAdapter != null) { // in case there is no NFC
            try {
                // create an NDEF message containing the current URL:
                NdefRecord rec = NdefRecord.createUri(url); // url: current URL (String or Uri)
                NdefMessage ndef = new NdefMessage(rec);
                // make it available via Android Beam:
                nfcAdapter.setNdefPushMessage(ndef, this, this);
            } catch (IllegalStateException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onPageFinished(String url) {
        progressBarContainer.animate()
                .alpha(0f)
                .setDuration(getResources().getInteger(android.R.integer.config_longAnimTime))
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        progressBarContainer.setVisibility(View.GONE);
                    }
                });
        if (refreshLayout.isRefreshing()) {
            refreshLayout.setRefreshing(false);
        }
    }

    //WebView factory methods below
    @Override
    public void onPageStarted(String url, Bitmap favicon) {
        if (!url.contains("http://www.apkmirror.com/wp-content/")) {
            runAsync(url);
            setupNFC(url);

            //Updating bottom navigation
            if (navigation.getCurrentTabId() == R.id.navigation_home) {
                if (url.equals(APKMIRROR_UPLOAD_URL)) {
                    triggerAction = false;
                    navigation.selectTabWithId(R.id.navigation_upload);
                }
            } else if (navigation.getCurrentTabId() == R.id.navigation_upload) {
                if (!url.equals(APKMIRROR_UPLOAD_URL)) {
                    triggerAction = false;
                    navigation.selectTabWithId(R.id.navigation_home);
                }
            }

            //Showing progress bar
            progressBarContainer.animate()
                    .alpha(1f)
                    .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime))
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationStart(Animator a) {
                            super.onAnimationStart(a);
                            progressBarContainer.setVisibility(View.VISIBLE);
                        }
                    });
        }
    }

    @Override
    public void onPageError(int eC, String d, String fU) { // eC = errorCode | d = description | fU = failingUrl
        if (eC == -2) {
            new MaterialDialog.Builder(this)
                    .title(R.string.error)
                    .content(getString(R.string.error_while_loading_page) + " " + fU +
                            "(" + String.valueOf(eC) + " " + d + ")")
                    .positiveText(R.string.refresh)
                    .negativeText(android.R.string.cancel)
                    .neutralText(R.string.dismiss)
                    .onPositive((dialog, which) -> {
                        webView.reload();
                        dialog.dismiss();
                    })
                    .onNegative((dialog, which) -> finish())
                    .onNeutral((materialDialog, dialogAction) -> materialDialog.dismiss())
                    .show();
        }
    }

    @Override
    public void onDownloadRequested(String url, String sF, String mimeType, /* sF = suggestedFilename */
                                    long contentLength, String contentDisposition, String userAgent) {

        if (isWritePermissionGranted()) {
            download(url, sF);
        } else {
            new MaterialDialog.Builder(MainActivity.this)
                    .title(R.string.write_permission)
                    .content(R.string.storage_access)
                    .positiveText(R.string.request_permission)
                    .negativeText(android.R.string.cancel)
                    .onPositive((dialog, which) -> {
                        //Request permission
                        ActivityCompat.requestPermissions(MainActivity.this, new String[]{
                                Manifest.permission.WRITE_EXTERNAL_STORAGE
                        }, 1);
                    })
                    .show();
        }
    }

    @Override
    public void onExternalPageRequest(String url) {
        Intent bI = new Intent(Intent.ACTION_VIEW, Uri.parse(url)); /* bI = browserIntent */
        startActivity(bI);
    }
}
