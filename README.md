# APKMirror

An unofficial APKMirror client/web application (forked from the original, which is now abandoned).

### Install
<a href="https://f-droid.org/packages/taco.apkmirror">
    <img src="https://f-droid.org/badge/get-it-on.png"
         alt="Get it on F-Droid" height="80">
</a>

### Contributions
Anyone may contribute (I need better translations, and better code review because I actually know next to nothing about coding).
Make a pull request with changes to contribute I guess...

### Features
- Quick loading (for the most part)
- Choose any download manager
- Clean material design (I think)
- Small-ish APK size
- Completely unrelated, but technically counts as a feature (and also for devices with kinda older versions of Android like Lollipop (maybe Marshmallow), and lower). You can connect to WiFis that require you to connect via a portal, JUST by launching the app (it's an "open via" prompt, just select any browser and it'll bring up the site)! Isn't that so convenient? I didn't even code that; I had no idea this was possible until I launched the app at my school (which requires a portal login)

### Things that constitute as Anti-Features
- The app itself does NOT contain any ads whatsoever. However, as anyone who has visited the 
APKMirror site probably knows, they do display ads. As this app utilizes WebView, the site's ads will
also end up being displayed in the app.

### Screenshots
Currently getting replacement screenshots for a more accurate representation.

### Credits
I decided to continue developing this app myself, as vojta-horanek has stopped development on it.

Original app by vojta-horanek: https://github.com/vojta-horanek/APKMirror

The last version (3.5) from vojta-horanek can be found here at XDA Labs: https://labs.xda-developers.com/store/app/cf.vojtechh.apkmirror

### License
This application is under the GNU General Public License, as licensed by the original creator.
```
APKMirror web app/client
    Copyright (C) 2017 Vojtěch Hořánek

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
```
